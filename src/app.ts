console.log("Heap sort");
import { sort } from "./heapSort";
import { performance } from "perf_hooks";

const getRandomNumber = (min: number, max: number) => {
  return Math.random() * (max - min) + min;
};

const generateRandomArray = () => {
  const numbers: number[] = [];

  while (numbers.length !== 1000000) {
    numbers.push(getRandomNumber(1, 100000000));
  }
  return numbers;
};

const main = async () => {
  const timesHeap: number[] = [];
  const timesSort: number[] = [];

  for (let i = 0; i <= 10; i++) {
    const numbers = generateRandomArray();

    var t0 = performance.now();
    const sortedNumbers = await sort(numbers);
    var t1 = performance.now();

    timesHeap.push(t1 - t0);

    // console.log("result", sortedNumbers);
  }

  const avrgTimeHeap = timesHeap.reduce((sum, n) => sum + n) / timesHeap.length;
  console.log("avg run time is", avrgTimeHeap / 1000, "s");

  console.log("sort");
  for (let i = 0; i <= 10; i++) {
    const numbers = generateRandomArray();

    var t0 = performance.now();
    const sortedNumbers = numbers.sort((a, b) => a - b);
    var t1 = performance.now();

    timesSort.push(t1 - t0);

    // console.log("result", sortedNumbers);
  }

  const avrgTimeSort = timesSort.reduce((sum, n) => sum + n) / timesSort.length;

  console.log("avg run time is", avrgTimeSort / 1000, "s");
};

main().catch((err) => console.error(err));
