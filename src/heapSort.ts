const heapify = (arr: number[], len: number, parentIndex: number) => {
  let largest = parentIndex;
  let left = parentIndex * 2 + 1;
  let right = left + 1;

  if (left < len && arr[left] > arr[largest]) {
    largest = left;
  }

  if (right < len && arr[right] > arr[largest]) {
    largest = right;
  }

  if (largest !== parentIndex) {
    [arr[parentIndex], arr[largest]] = [arr[largest], arr[parentIndex]];

    heapify(arr, len, largest);
  }

  return arr;
};

export const sort = (numbers: number[]) => {
  return new Promise((resolve, reject) => {
    let length = numbers.length;
    let lastParentNode = Math.floor(length / 2 - 1);
    let lastChild = length - 1;

    while (lastParentNode >= 0) {
      heapify(numbers, length, lastParentNode);
      lastParentNode--;
    }

    while (lastChild >= 0) {
      [numbers[0], numbers[lastChild]] = [numbers[lastChild], numbers[0]];
      heapify(numbers, lastChild, 0);
      lastChild--;
    }

    resolve(numbers);
  });
};
